package com.concensustechnologies.restlettest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.restlet.Context;
import org.restlet.Response;
import org.restlet.data.ChallengeResponse;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.restlet.util.Series;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


public class CanvasRestClient {

	private String restBaseURI;
	private String accessToken;
	private String orgID;
	private ObjectMapper mapper; //Jackson mapper to serialize/deserialize JSON
	private Context context; //restlet context
	private static String accounts_segment = "/api/v1/accounts";
	private static String users_segment = "/api/v1/users";
	private static String courses_segment = "/api/v1/courses";
	public static final String DELIM_LNK = ",";
	public static final String DELIM_PARM = ";";

	public CanvasRestClient(String restBaseURI, String accessToken) {
		this.restBaseURI = restBaseURI;
		this.accessToken = accessToken;
		this.orgID = "1";
		mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
		context = new Context();

	}

	private Response getResponse(String URI, String token) throws ResourceException, JsonParseException, JsonMappingException, IOException, CanvasException {

		
		if ((URI !=null) && (token != null)) {
			ClientResource client = new ClientResource(URI);
			ChallengeResponse cr = new ChallengeResponse(ChallengeScheme.HTTP_OAUTH_BEARER);
			cr.setRawValue(token);
			client.setChallengeResponse(cr);
			client.get();
			Response resp = client.getResponse();
			
			//test response status
			if (resp.getStatus().isSuccess()) {
				//Test media type, should be JSON
				String mType = resp.getEntity().getMediaType().getName();
				System.out.println("Media type " + mType);
				return resp;
			} else {
				//Attempt to recover an error
				
				String errorJSON = resp.getEntityAsText();
				
				
				throw new CanvasException(errorJSON);
				
			}
			
			

		} else {
			return null;
		}

	}

	private List<CanvasUser> getAllUsers(String URI, String token) throws JsonParseException, JsonMappingException, IOException, ResourceException, CanvasException{

		List<CanvasUser> users = new ArrayList<CanvasUser>();
		ObjectMapper mapper;
		mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);

		if ((URI != null) && (token != null)) {
			URI = URI + "?per_page=1000";
			Response resp = getResponse(URI, token);
			Series<Header> headers = (Series<Header>) resp.getAttributes().get("org.restlet.http.headers");
			Header linkHDR = headers.getFirst("Link");
			Map<String, String> linkMap = this.convertLinkHDRMap(linkHDR);

			List<CanvasUser> listPart = Arrays.asList(mapper.readValue(resp.getEntityAsText(), CanvasUser[].class));
			System.out.println(listPart.size());
			users.addAll(listPart);

			while (linkMap.containsKey("next")) {
				URI = linkMap.get("next");
				resp = getResponse(URI, token);
				headers = (Series<Header>) resp.getAttributes().get("org.restlet.http.headers");
				linkHDR = headers.getFirst("Link");
				linkMap = convertLinkHDRMap(linkHDR);

				listPart = Arrays.asList(mapper.readValue(resp.getEntityAsText(), CanvasUser[].class));

				users.addAll(listPart);

			}
		}


		return users;

	}

	private Map<String,String> convertLinkHDRMap(Header links){

		Map<String, String> pLinks = new HashMap<String, String>();

		String rawLinks = links.getValue();

		if (rawLinks != null) {
			String[] params = rawLinks.split(DELIM_LNK);

			for (String element : params ) {

				String[] segments = element.split(DELIM_PARM);

				//System.out.println("ELEMENT is " + element);

				if (segments.length < 2) {
					//not well formed or what we expect
					continue;
				}

				//extract URI portion. Should be <uri>

				String uriPart = segments[0].trim();

				//System.out.println("URI Part " + uriPart);


				if (!uriPart.startsWith("<") || (!uriPart.endsWith(">"))) {
					//not well formed
					continue;
				}

				uriPart = uriPart.substring(1,uriPart.length() - 1); //strip angle braces

				for (int i = 1; i < segments.length; i++ ) {
					//there may be more than one rel, collect them

					//System.out.println("Segments element i " + segments[i]);
					String[] rel = segments[i].trim().split("="); //form is relName = value in quotes

					//System.out.println("REL "+ rel[0] + " " + rel[1]);
					if (rel.length < 2) {
						//no good
						continue;
					}

					String value = rel[1].trim();

					//System.out.println("value " + value);

					if (value.startsWith("\"") && value.endsWith("\"")) {
						value = value.substring(1, value.length() - 1);
					}

					pLinks.put(value, uriPart);

				}



			}
		}

		return pLinks;



	}
	
	public CanvasUser createUser(String URI, String token, CanvasUser user) throws JsonParseException, JsonMappingException, IOException {
		
		CanvasUser retUsr = null;
		
		
		if ((user!=null)&&(URI!=null)&&(token!=null)) {
			
			ClientResource client = new ClientResource(URI);
			ChallengeResponse cr = new ChallengeResponse(ChallengeScheme.HTTP_OAUTH_BEARER);
			cr.setRawValue(token);
			client.setChallengeResponse(cr);
			
			
			Map<String,String> cattrMap = user.getCreationAttributes();
			
			Set<String> cattrs = cattrMap.keySet();
			
			Form parameters = new Form();
				
			for (String attr : cattrs) {
				if (cattrMap.get(attr)!=null) {
					parameters.add(attr, cattrMap.get(attr));
				}
			}
			
			client.post(parameters);
			Response resp = client.getResponse();
			
			if(resp.isEntityAvailable()){
				System.out.println(resp.getEntityAsText());
				retUsr = mapper.readValue(resp.getEntityAsText(), CanvasUser.class);
			}
			
		}
		
		return retUsr;
		
	}


	public CanvasUser getUser(String URI, String token, String id) throws JsonParseException, JsonMappingException, IOException {
		CanvasUser retUsr = null;
		
		if ((URI!=null)&&(token!=null)&&(id!=null)){
			URI = URI+"/"+id;
			ClientResource client = new ClientResource(URI);
			ChallengeResponse cr = new ChallengeResponse(ChallengeScheme.HTTP_OAUTH_BEARER);
			cr.setRawValue(token);
			client.setChallengeResponse(cr);
			
			client.get();
			Response resp = client.getResponse();
			
			if (resp.isEntityAvailable()) {
				System.out.println(resp.getEntityAsText());
				retUsr = mapper.readValue(resp.getEntityAsText(), CanvasUser.class);
			}
			
		}
		
		return retUsr;
	}
	
	public Status deleteUser(String URI, String token, String id) {
		
		if ((URI!=null)&&(token!=null)&&(id!=null)){
			URI = URI+"/"+id;
			ClientResource client = new ClientResource(URI);
			ChallengeResponse cr = new ChallengeResponse(ChallengeScheme.HTTP_OAUTH_BEARER);
			cr.setRawValue(token);
			client.setChallengeResponse(cr);
			
			client.delete();
			Response resp = client.getResponse();
			
			return resp.getStatus();
			
			
		}
		
		return null;
	}
	
	public List<CanvasLogin> getUserLogins(String baseURI, String token, String id) throws JsonParseException, JsonMappingException, ResourceException, IOException, CanvasException {
		List<CanvasLogin> logins = new ArrayList<CanvasLogin>();
		ObjectMapper mapper;
		mapper = new ObjectMapper();
		mapper.disable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);

		if ((baseURI != null) && (token != null)) {
			String URI = baseURI + "/api/v1/users/" + id + "/logins" + "?per_page=1000";
			Response resp = getResponse(URI, token);
			Series<Header> headers = (Series<Header>) resp.getAttributes().get("org.restlet.http.headers");
			Header linkHDR = headers.getFirst("Link");
			Map<String, String> linkMap = this.convertLinkHDRMap(linkHDR);

			List<CanvasLogin> listPart = Arrays.asList(mapper.readValue(resp.getEntityAsText(), CanvasLogin[].class));
			System.out.println(listPart.size());
			logins.addAll(listPart);

			while (linkMap.containsKey("next")) {
				URI = linkMap.get("next");
				resp = getResponse(URI, token);
				headers = (Series<Header>) resp.getAttributes().get("org.restlet.http.headers");
				linkHDR = headers.getFirst("Link");
				linkMap = convertLinkHDRMap(linkHDR);
				
				System.out.println(resp.getEntityAsText());

				listPart = Arrays.asList(mapper.readValue(resp.getEntityAsText(), CanvasLogin[].class));

				logins.addAll(listPart);

			}
		}

		return logins;
	
	}
	
	public Status updateUser(String baseURI, String token, String id, CanvasUser user) {
		// leave null that which shouldn't be updated
		
		//updates name, short_name, sortable_name, time_zone, email, locale
		
		if ((baseURI!=null)&&(token!=null)&&(id!=null)&&(user!=null)){
			String URI = baseURI + "/api/v1/users/" + id;
			
			ClientResource client = new ClientResource(URI);
			ChallengeResponse cr = new ChallengeResponse(ChallengeScheme.HTTP_OAUTH_BEARER);
			cr.setRawValue(token);
			client.setChallengeResponse(cr);
			
			Form parameters = new Form();
			
			if (user.getName()!=null) {
				parameters.add("user[name]",user.getName());				
			}
			
			if (user.getShortName()!=null) {
				parameters.add("user[short_name]", user.getShortName());
			}
			
			if (user.getSortableName()!=null) {
				parameters.add("user[sortable_name]", user.getSortableName());
			}
			
			if (user.getTimeZone()!=null) {
				parameters.add("user[time_zone]", user.getTimeZone());
			}
			
			if (user.getEmail()!=null) {
				parameters.add("user[email]", user.getEmail());
			}
			
			if (user.getLocale()!=null) {
				parameters.add("user[locale]", user.getLocale());
			}
			
			if (parameters.isEmpty()) {
				//nothing to update
				return null;
			} else {
				
			 client.put(parameters);
			 Response resp = client.getResponse();
			 return resp.getStatus();
				
			}
		} 
		
		return null;
	}
	
	public Status updateLogin(String baseURI, String token, CanvasLogin login) {
		
		// leave null that which shouldn't be updated
		
		//updates unique_id, password, sis_user_id, and integration_id for an existing login
		
		if ((baseURI!=null)&&(token!=null)&&(login!=null)){
			String URI = baseURI + "/api/v1/accounts/" + orgID + "/logins/" + login.getId().toString();
			
			ClientResource client = new ClientResource(URI);
			ChallengeResponse cr = new ChallengeResponse(ChallengeScheme.HTTP_OAUTH_BEARER);
			cr.setRawValue(token);
			client.setChallengeResponse(cr);
			
			Form parameters = new Form();
			
			if (login.getUniqueId()!=null) {
				parameters.add("login[unique_id]",login.getUniqueId());				
			}
			
			if (login.getSisUserId()!=null) {
				parameters.add("login[sis_user_id]", login.getSisUserId().toString());
			}
			
			if (login.getPassword()!=null) {
				parameters.add("login[password]", login.getPassword());
			}
			
			if (login.getIntegrationId()!=null) {
				parameters.add("login[integration_id]", login.getIntegrationId().toString());
			}
			
			if (login.getAuthenticationProviderId()!=null) {
				parameters.add("login[authenticatino_provider_id]", login.getAuthenticationProviderId().toString());
			}
			
			if (parameters.isEmpty()) {
				//nothing to update
				return null;
			} else {
				
			 client.put(parameters);
			 Response resp = client.getResponse();
			 return resp.getStatus();
				
			}
		} 
		
		return null;
		
	}
}
