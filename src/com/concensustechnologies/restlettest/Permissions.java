package com.concensustechnologies.restlettest;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"can_update_name",
"can_update_avatar"
})
public class Permissions {

@JsonProperty("can_update_name")
private Boolean canUpdateName;
@JsonProperty("can_update_avatar")
private Boolean canUpdateAvatar;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("can_update_name")
public Boolean getCanUpdateName() {
return canUpdateName;
}

@JsonProperty("can_update_name")
public void setCanUpdateName(Boolean canUpdateName) {
this.canUpdateName = canUpdateName;
}

@JsonProperty("can_update_avatar")
public Boolean getCanUpdateAvatar() {
return canUpdateAvatar;
}

@JsonProperty("can_update_avatar")
public void setCanUpdateAvatar(Boolean canUpdateAvatar) {
this.canUpdateAvatar = canUpdateAvatar;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public String toString() {
return new ToStringBuilder(this).append("canUpdateName", canUpdateName).append("canUpdateAvatar", canUpdateAvatar).append("additionalProperties", additionalProperties).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(additionalProperties).append(canUpdateAvatar).append(canUpdateName).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof Permissions) == false) {
return false;
}
Permissions rhs = ((Permissions) other);
return new EqualsBuilder().append(additionalProperties, rhs.additionalProperties).append(canUpdateAvatar, rhs.canUpdateAvatar).append(canUpdateName, rhs.canUpdateName).isEquals();
}

}