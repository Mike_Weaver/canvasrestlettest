package com.concensustechnologies.restlettest;

import org.restlet.data.Status;

/**
 *
 * @author Michael Weaver <Mike.Weaver@concensus.com>
 */
public class CanvasException extends Exception{
    private Status status = null;

    public CanvasException() {
    }

    public CanvasException(String message) {
        super(message);
    }

    public CanvasException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public Status getStatus(){
        return status;
    }
    
    public void setStatus(Status status){
        this.status = status;
    }
}