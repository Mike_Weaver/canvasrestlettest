package com.concensustechnologies.restlettest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.restlet.Response;
import org.restlet.Uniform;
import org.restlet.data.ChallengeResponse;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.restlet.util.Series;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;



public class RestLetTest {
	
	public static final String DELIM_LNK = ",";
	public static final String DELIM_PARM = ";";


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String accountsURI = "https://mountaloysius.test.instructure.com/api/v1/accounts";
		String userListURI = "https://mountaloysius.test.instructure.com/api/v1/accounts/1/users";
		String userGetURI  = "https://mountaloysius.test.instructure.com/api/v1/users";
		String accessToken = "11163~dMnF6GtvwhOfqE9F8GRB5b6WDOvJ9jnD96c2G5fomYRohk6o4T5h6VsJUOIniBt1";
		String restBaseURI = "https://mountaloysius.test.instructure.com";
		
		CanvasRestClient client = new CanvasRestClient(restBaseURI, accessToken);
		
		
		try {
			
			//Response resp = getResponse(userListURI, accessToken);
			//Series<Header> headers = (Series<Header>) resp.getAttributes().get("org.restlet.http.headers");
			//Series<Header> headers = (Series<Header>) client.getResponseAttributes().get("org.restlet.http.headers");

			//System.out.println(headers.getFirstValue("Link"));
			//Header linkHDR = headers.getFirst("Link");
			//Map<String, String> linkMap = convertLinkHDRMap(linkHDR);
			
			//System.out.println("Do it have a next? " + linkMap.containsKey("next"));
			
			//System.out.println(resp.getEntityAsText());

			
			
			//if (linkMap.containsKey("next")) {
			//	System.out.println("The next URI is " + linkMap.get("next"));
				//try to get next page
			//	resp = getResponse(linkMap.get("next"), accessToken);
				
			//	System.out.println(resp.getEntityAsText());
			//	
				
			//}
			
			List<CanvasUser> users = getAllUsers(userListURI, accessToken);
			
			System.out.println("User count is " + users.size());
			
			printUsers(users);
			
			//CanvasUser boy = new CanvasUser();
			
			//boy.setName("WeaverTest3");
			//boy.setShortName("WeaverTest3");
			//boy.setLoginId("weavertest3@mtaloy.edu");
			
			//CanvasUser resultBoy = client.createUser(userListURI, accessToken, boy);
			
			//System.out.println("Result boy ID is :" + resultBoy.getId().toString());
			
			//String id = "722";
			
			//List<CanvasLogin> logins = client.getUserLogins(restBaseURI, accessToken, id);
			
			//String testval = "weavertest3@mtaloy.edu";
			
			//for (CanvasLogin login : logins) {
			//	System.out.println(login.toString());
			//	if (login.getUniqueId().equalsIgnoreCase(testval)) {
			//		System.out.println("found matching login");
			//		login.setUniqueId("weavertest4@mtaloy.edu");
			//		Status status = client.updateLogin(restBaseURI, accessToken, login);
			//		System.out.println(status.toString());
			//	}
			//}
			
			//Status status  = client.deleteUser(userListURI, accessToken, id);
			
			//System.out.println(status.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
	
	private static Response getResponse(String URI, String token) throws ResourceException {
		
		if ((URI !=null) && (token != null)) {
			ClientResource client = new ClientResource(URI);
			ChallengeResponse cr = new ChallengeResponse(ChallengeScheme.HTTP_OAUTH_BEARER);
			cr.setRawValue(token);
			client.setChallengeResponse(cr);
			client.get();
			Response resp = client.getResponse();
			return resp;
			
		} else {
			return null;
		}
				
	}
	
	private static List<CanvasUser> getAllUsers(String URI, String token) throws JsonParseException, JsonMappingException, IOException{
		
		List<CanvasUser> users = new ArrayList<CanvasUser>();
		ObjectMapper mapper;
		mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
		
		if ((URI != null) && (token != null)) {
			URI = URI + "?per_page=1000";
			Response resp = getResponse(URI, token);
			Series<Header> headers = (Series<Header>) resp.getAttributes().get("org.restlet.http.headers");
			Header linkHDR = headers.getFirst("Link");
			Map<String, String> linkMap = convertLinkHDRMap(linkHDR);
			
			List<CanvasUser> listPart = Arrays.asList(mapper.readValue(resp.getEntityAsText(), CanvasUser[].class));
			System.out.println(listPart.size());
			users.addAll(listPart);
			
			while (linkMap.containsKey("next")) {
				URI = linkMap.get("next");
				resp = getResponse(URI, token);
				headers = (Series<Header>) resp.getAttributes().get("org.restlet.http.headers");
				linkHDR = headers.getFirst("Link");
				linkMap = convertLinkHDRMap(linkHDR);
				
				listPart = Arrays.asList(mapper.readValue(resp.getEntityAsText(), CanvasUser[].class));
				
				users.addAll(listPart);
				
			}
		}
		
		
		return users;
		
	}
	
	private boolean hasNext(Series linkHeader) {
		return true;
	}
	
	private static List<Link> convertLinkHDR(Header links){
		
		List<Link> pLinks = new ArrayList();
		
		String rawLinks = links.getValue();
		
		if (rawLinks != null) {
			String[] params = rawLinks.split(DELIM_LNK);
			
			for (String element : params ) {
				
				String[] segments = element.split(DELIM_PARM);
				
				System.out.println("ELEMENT is " + element);
				
				if (segments.length < 2) {
					//not well formed or what we expect
					continue;
				}
				
				//extract URI portion. Should be <uri>
				
				String uriPart = segments[0].trim();
				
				System.out.println("URI Part " + uriPart);
				
				
				if (!uriPart.startsWith("<") || (!uriPart.endsWith(">"))) {
					//not well formed
					continue;
				}
				
				uriPart = uriPart.substring(1,uriPart.length() - 1); //strip angle braces
				
				for (int i = 1; i < segments.length; i++ ) {
					//there may be more than one rel, collect them
					
					System.out.println("Segments element i " + segments[i]);
					String[] rel = segments[i].trim().split("="); //form is relName = value in quotes
					
					System.out.println("REL "+ rel[0] + " " + rel[1]);
					if (rel.length < 2) {
						//no good
						continue;
					}
					
					String value = rel[1].trim();
					
					System.out.println("value " + value);
					
					if (value.startsWith("\"") && value.endsWith("\"")) {
						value = value.substring(1, value.length() - 1);
					}
					
					System.out.println("value prior to lnk " + value);
					Link lnk = new Link(uriPart,value);
					System.out.println(lnk.getREL());
					pLinks.add(lnk);
				}
				
				
				
			}
		}
		
		return pLinks;
		
		
		
	}
	
private static Map<String,String> convertLinkHDRMap(Header links){
		
		Map<String, String> pLinks = new HashMap<String, String>();
		
		String rawLinks = links.getValue();
		
		if (rawLinks != null) {
			String[] params = rawLinks.split(DELIM_LNK);
			
			for (String element : params ) {
				
				String[] segments = element.split(DELIM_PARM);
				
				//System.out.println("ELEMENT is " + element);
				
				if (segments.length < 2) {
					//not well formed or what we expect
					continue;
				}
				
				//extract URI portion. Should be <uri>
				
				String uriPart = segments[0].trim();
				
				//System.out.println("URI Part " + uriPart);
				
				
				if (!uriPart.startsWith("<") || (!uriPart.endsWith(">"))) {
					//not well formed
					continue;
				}
				
				uriPart = uriPart.substring(1,uriPart.length() - 1); //strip angle braces
				
				for (int i = 1; i < segments.length; i++ ) {
					//there may be more than one rel, collect them
					
					//System.out.println("Segments element i " + segments[i]);
					String[] rel = segments[i].trim().split("="); //form is relName = value in quotes
					
					//System.out.println("REL "+ rel[0] + " " + rel[1]);
					if (rel.length < 2) {
						//no good
						continue;
					}
					
					String value = rel[1].trim();
					
					//System.out.println("value " + value);
					
					if (value.startsWith("\"") && value.endsWith("\"")) {
						value = value.substring(1, value.length() - 1);
					}
					
					pLinks.put(value, uriPart);

				}
				
				
				
			}
		}
		
		return pLinks;
		
		
		
	}
	
	private static void printLinks(List<Link> links) {
		
		for (Link lnk : links) {
			System.out.println("Rel is " + lnk.getREL() + "  URI is " + lnk.getURI());
		}
	}
	
	private static void printUsers(List<CanvasUser> users) {
		
		for (CanvasUser user : users) {
			System.out.println("User "+ user.getShortName() + "User's ID "+ user.getId().toString());
		}
	}
	
	

}
