package com.concensustechnologies.restlettest;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"user_id",
"account_id",
"unique_id",
"sis_user_id",
"integration_id",
"authentication_provider_id"
})
public class CanvasLogin {

@JsonProperty("id")
private Integer id;
@JsonProperty("user_id")
private Integer userId;
@JsonProperty("account_id")
private Integer accountId;
@JsonProperty("unique_id")
private String uniqueId;
@JsonProperty("sis_user_id")
private Object sisUserId;
@JsonProperty("integration_id")
private Object integrationId;
@JsonProperty("authentication_provider_id")
private Object authenticationProviderId;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();
@JsonIgnore
private String password;

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("user_id")
public Integer getUserId() {
return userId;
}

@JsonProperty("user_id")
public void setUserId(Integer userId) {
this.userId = userId;
}

@JsonProperty("account_id")
public Integer getAccountId() {
return accountId;
}

@JsonProperty("account_id")
public void setAccountId(Integer accountId) {
this.accountId = accountId;
}

@JsonProperty("unique_id")
public String getUniqueId() {
return uniqueId;
}

@JsonProperty("unique_id")
public void setUniqueId(String uniqueId) {
this.uniqueId = uniqueId;
}

@JsonProperty("sis_user_id")
public Object getSisUserId() {
return sisUserId;
}

@JsonProperty("sis_user_id")
public void setSisUserId(Object sisUserId) {
this.sisUserId = sisUserId;
}

@JsonProperty("integration_id")
public Object getIntegrationId() {
return integrationId;
}

@JsonProperty("integration_id")
public void setIntegrationId(Object integrationId) {
this.integrationId = integrationId;
}

@JsonProperty("authentication_provider_id")
public Object getAuthenticationProviderId() {
return authenticationProviderId;
}

@JsonProperty("authentication_provider_id")
public void setAuthenticationProviderId(Object authenticationProviderId) {
this.authenticationProviderId = authenticationProviderId;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

/**
 * @return the password
 */
public String getPassword() {
	return password;
}

/**
 * @param password the password to set
 */
public void setPassword(String password) {
	this.password = password;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("id", id).append("userId", userId).append("accountId", accountId).append("uniqueId", uniqueId).append("sisUserId", sisUserId).append("integrationId", integrationId).append("authenticationProviderId", authenticationProviderId).append("additionalProperties", additionalProperties).toString();
}

@Override
public int hashCode() {
return new HashCodeBuilder().append(id).append(accountId).append(additionalProperties).append(userId).append(integrationId).append(sisUserId).append(authenticationProviderId).append(uniqueId).toHashCode();
}

@Override
public boolean equals(Object other) {
if (other == this) {
return true;
}
if ((other instanceof CanvasLogin) == false) {
return false;
}
CanvasLogin rhs = ((CanvasLogin) other);
return new EqualsBuilder().append(id, rhs.id).append(accountId, rhs.accountId).append(additionalProperties, rhs.additionalProperties).append(userId, rhs.userId).append(integrationId, rhs.integrationId).append(sisUserId, rhs.sisUserId).append(authenticationProviderId, rhs.authenticationProviderId).append(uniqueId, rhs.uniqueId).isEquals();
}

}
