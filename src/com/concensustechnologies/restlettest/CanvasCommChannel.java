package com.concensustechnologies.restlettest;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"id",
	"position",
	"user_id",
	"workflow_state",
	"address",
	"type"
})
public class CanvasCommChannel {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("position")
	private Integer position;
	@JsonProperty("user_id")
	private Integer userId;
	@JsonProperty("workflow_state")
	private String workflowState;
	@JsonProperty("address")
	private String address;
	@JsonProperty("type")
	private String type;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("position")
	public Integer getPosition() {
		return position;
	}

	@JsonProperty("position")
	public void setPosition(Integer position) {
		this.position = position;
	}

	@JsonProperty("user_id")
	public Integer getUserId() {
		return userId;
	}

	@JsonProperty("user_id")
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@JsonProperty("workflow_state")
	public String getWorkflowState() {
		return workflowState;
	}

	@JsonProperty("workflow_state")
	public void setWorkflowState(String workflowState) {
		this.workflowState = workflowState;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("position", position).append("userId", userId).append("workflowState", workflowState).append("address", address).append("type", type).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(position).append(id).append(workflowState).append(address).append(additionalProperties).append(userId).append(type).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof CanvasCommChannel) == false) {
			return false;
		}
		CanvasCommChannel rhs = ((CanvasCommChannel) other);
		return new EqualsBuilder().append(position, rhs.position).append(id, rhs.id).append(workflowState, rhs.workflowState).append(address, rhs.address).append(additionalProperties, rhs.additionalProperties).append(userId, rhs.userId).append(type, rhs.type).isEquals();
	}

}
