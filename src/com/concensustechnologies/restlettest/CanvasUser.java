package com.concensustechnologies.restlettest;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"id",
	"name",
	"sortable_name",
	"short_name",
	"sis_user_id",
	"sis_import_id",
	"integration_id",
	"login_id",
	"avatar_url",
	"enrollments",
	"email",
	"locale",
	"last_login",
	"time_zone",
	"bio"
})
public class CanvasUser {

	@JsonProperty("id")
	private BigInteger id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("sortable_name")
	private String sortableName;
	@JsonProperty("short_name")
	private String shortName;
	@JsonProperty("sis_user_id")
	private String sisUserId;
	@JsonProperty("sis_import_id")
	private Integer sisImportId;
	@JsonProperty("integration_id")
	private String integrationId;
	@JsonProperty("login_id")
	private String loginId;
	@JsonProperty("avatar_url")
	private String avatarUrl;
	@JsonProperty("enrollments")
	private Object enrollments;
	@JsonProperty("email")
	private String email;
	@JsonProperty("locale")
	private String locale;
	@JsonProperty("last_login")
	private String lastLogin;
	@JsonProperty("time_zone")
	private String timeZone;
	@JsonProperty("bio")
	private String bio;
	@JsonProperty("permissions")
	private Permissions permissions;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonIgnore
	private boolean termsOfUse;
	@JsonIgnore
	private boolean skipRegistration;
	@JsonIgnore
	private String userPassword;
	@JsonIgnore
	private boolean sendConfirmation;
	@JsonIgnore
	private boolean forceSelfReg;
	@JsonIgnore
	private String authProvider;
	@JsonIgnore
	private String commChannelType;
	@JsonIgnore
	private String commChannelAddress;
	@JsonIgnore
	private boolean commChannelSkipConfirmation;
	@JsonIgnore
	private boolean forceValidations;
	@JsonIgnore
	private boolean enableSISReactivation;

	private List<String> cAttrs = Arrays.asList(
			"name",
			"shortName",
			"sortableName",
			"timeZone",
			"locale",
			"termsOfUse",
			"skipRegistration",
			"loginId",
			"userPassword",
			"sisUserId",
			"integrationId",
			"sendConfirmation",
			"forceSelfReg",
			"authProvider",
			"commChannelType",
			"commChannelAddress",
			"commChannelSkipConfirmation",
			"forceValidations",
			"enableSISReactivation");

	private Map<String, String> cAttrHDRNames;

	public CanvasUser(){
		this.cAttrHDRNames = this.createCAttrHdrMap();
	}

	private Map<String, String> createCAttrHdrMap() {
		Map<String, String> result = new HashMap();

		result.put("name", "user[name]");
		result.put("shortName", "user[short_name]");
		result.put("sortableName", "user[sortable_name]");
		result.put("timeZone", "user[time_zone]");
		result.put("locale","user[locale]");
		result.put("termsOfUse", "user[terms_of_use]");
		result.put("skipRegistration", "user[skip_registration]");

		result.put("loginId", "pseudonym[unique_id]");
		result.put("userPassword", "pseudonym[password]");
		result.put("sisUserId", "pseudonym[sis_user_id]");
		result.put("integrationId", "pseudonym[integration_id]");
		result.put("sendConfirmation", "pseudonym[send_confirmation]");
		result.put("forceSelfReg", "pseudonym[force_self_registration]");
		result.put("authProvider", "pseudonym[authentication_provider_id]");

		result.put("commChannelType", "communication_channel[type]");
		result.put("commChannelAddress", "communication_channel[address]");
		result.put("commChannelSkipConfirmation", "communication_channel[skip_confirmation]");

		result.put("forceValidations", "force_validations");
		result.put("enableSISReactivation", "enable_sis_reactivation");

		return result;
	}

	public Map<String,String> getCreationAttributes(){
		Map<String,String> result = new HashMap();
		
		result.put("user[name]", this.name);
		result.put("user[short_name]", this.shortName);
		result.put("user[sortable_name]", this.sortableName);
		result.put("user[time_zone]", this.timeZone);
		result.put("user[locale]", this.locale);
		result.put("user[terms_of_use]", Boolean.toString(this.termsOfUse));
		result.put("user[skip_registration]", Boolean.toString(this.skipRegistration));
		result.put("pseudonym[unique_id]", this.loginId);
		result.put("pseudonym[password]", this.userPassword);
		result.put("pseudonym[sis_user_id]", this.sisUserId);
		result.put("pseudonym[integration_id]", this.integrationId);
		result.put("pseudonym[send_confirmation]", Boolean.toString(this.sendConfirmation));
		result.put("pseudonym[force_self_registration]", Boolean.toString(this.forceSelfReg));
		result.put("pseudonym[authentication_provider_id]", this.authProvider);
		result.put("communication_channel[type]", this.commChannelType);
		result.put("communication_channel[address]", this.commChannelAddress);
		result.put("communication_channel[skip_confirmation]", Boolean.toString(this.commChannelSkipConfirmation));
		result.put("force_validations", Boolean.toString(this.forceValidations));
		result.put("enable_sis_reactivation", Boolean.toString(this.enableSISReactivation));
		
		return result;
	}

	@JsonProperty("id")
	public BigInteger getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(BigInteger id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("sortable_name")
	public String getSortableName() {
		return sortableName;
	}

	@JsonProperty("sortable_name")
	public void setSortableName(String sortableName) {
		this.sortableName = sortableName;
	}

	@JsonProperty("short_name")
	public String getShortName() {
		return shortName;
	}

	@JsonProperty("short_name")
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	@JsonProperty("sis_user_id")
	public String getSisUserId() {
		return sisUserId;
	}

	@JsonProperty("sis_user_id")
	public void setSisUserId(String sisUserId) {
		this.sisUserId = sisUserId;
	}

	@JsonProperty("sis_import_id")
	public Integer getSisImportId() {
		return sisImportId;
	}

	@JsonProperty("sis_import_id")
	public void setSisImportId(Integer sisImportId) {
		this.sisImportId = sisImportId;
	}

	@JsonProperty("integration_id")
	public String getIntegrationId() {
		return integrationId;
	}

	@JsonProperty("integration_id")
	public void setIntegrationId(String integrationId) {
		this.integrationId = integrationId;
	}

	@JsonProperty("login_id")
	public String getLoginId() {
		return loginId;
	}

	@JsonProperty("login_id")
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	@JsonProperty("avatar_url")
	public String getAvatarUrl() {
		return avatarUrl;
	}

	@JsonProperty("avatar_url")
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	@JsonProperty("enrollments")
	public Object getEnrollments() {
		return enrollments;
	}

	@JsonProperty("enrollments")
	public void setEnrollments(Object enrollments) {
		this.enrollments = enrollments;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("locale")
	public String getLocale() {
		return locale;
	}

	@JsonProperty("locale")
	public void setLocale(String locale) {
		this.locale = locale;
	}

	@JsonProperty("last_login")
	public String getLastLogin() {
		return lastLogin;
	}

	@JsonProperty("last_login")
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	@JsonProperty("time_zone")
	public String getTimeZone() {
		return timeZone;
	}

	@JsonProperty("time_zone")
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	@JsonProperty("bio")
	public String getBio() {
		return bio;
	}

	@JsonProperty("bio")
	public void setBio(String bio) {
		this.bio = bio;
	}

	@JsonProperty("permissions")
	public Permissions getPermissions() {
		return permissions;
	}

	@JsonProperty("permissions")
	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/**
	 * @return the termsOfUse
	 */
	public boolean isTermsOfUse() {
		return termsOfUse;
	}

	/**
	 * @param termsOfUse the termsOfUse to set
	 */
	public void setTermsOfUse(boolean termsOfUse) {
		this.termsOfUse = termsOfUse;
	}

	/**
	 * @return the skipRegistration
	 */
	public boolean isSkipRegistration() {
		return skipRegistration;
	}

	/**
	 * @param skipRegistration the skipRegistration to set
	 */
	public void setSkipRegistration(boolean skipRegistration) {
		this.skipRegistration = skipRegistration;
	}

	/**
	 * @return the userPassword
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * @param userPassword the userPassword to set
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * @return the sendConfirmation
	 */
	public boolean isSendConfirmation() {
		return sendConfirmation;
	}

	/**
	 * @param sendConfirmation the sendConfirmation to set
	 */
	public void setSendConfirmation(boolean sendConfirmation) {
		this.sendConfirmation = sendConfirmation;
	}

	/**
	 * @return the forceSelfReg
	 */
	public boolean isForceSelfReg() {
		return forceSelfReg;
	}

	/**
	 * @param forceSelfReg the forceSelfReg to set
	 */
	public void setForceSelfReg(boolean forceSelfReg) {
		this.forceSelfReg = forceSelfReg;
	}

	/**
	 * @return the authProvider
	 */
	public String getAuthProvider() {
		return authProvider;
	}

	/**
	 * @param authProvider the authProvider to set
	 */
	public void setAuthProvider(String authProvider) {
		this.authProvider = authProvider;
	}

	/**
	 * @return the commChannelType
	 */
	public String getCommChannelType() {
		return commChannelType;
	}

	/**
	 * @param commChannelType the commChannelType to set
	 */
	public void setCommChannelType(String commChannelType) {
		this.commChannelType = commChannelType;
	}

	/**
	 * @return the commChannelAddress
	 */
	public String getCommChannelAddress() {
		return commChannelAddress;
	}

	/**
	 * @param commChannelAddress the commChannelAddress to set
	 */
	public void setCommChannelAddress(String commChannelAddress) {
		this.commChannelAddress = commChannelAddress;
	}

	/**
	 * @return the commChannelSkipConfirmation
	 */
	public boolean isCommChannelSkipConfirmation() {
		return commChannelSkipConfirmation;
	}

	/**
	 * @param commChannelSkipConfirmation the commChannelSkipConfirmation to set
	 */
	public void setCommChannelSkipConfirmation(boolean commChannelSkipConfirmation) {
		this.commChannelSkipConfirmation = commChannelSkipConfirmation;
	}

	/**
	 * @return the forceValidations
	 */
	public boolean isForceValidations() {
		return forceValidations;
	}

	/**
	 * @param forceValidations the forceValidations to set
	 */
	public void setForceValidations(boolean forceValidations) {
		this.forceValidations = forceValidations;
	}

	/**
	 * @return the enableSISReactivation
	 */
	public boolean isEnableSISReactivation() {
		return enableSISReactivation;
	}

	/**
	 * @param enableSISReactivation the enableSISReactivation to set
	 */
	public void setEnableSISReactivation(boolean enableSISReactivation) {
		this.enableSISReactivation = enableSISReactivation;
	}

	/**
	 * @param additionalProperties the additionalProperties to set
	 */
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	/**
	 * @return the cAttrs
	 */
	public List<String> getcAttrs() {
		return cAttrs;
	}

	/**
	 * @return the cAttrHDRNames
	 */
	public Map<String, String> getcAttrHDRNames() {
		return cAttrHDRNames;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("sortableName", sortableName).append("shortName", shortName).append("sisUserId", sisUserId).append("sisImportId", sisImportId).append("integrationId", integrationId).append("loginId", loginId).append("avatarUrl", avatarUrl).append("enrollments", enrollments).append("email", email).append("locale", locale).append("lastLogin", lastLogin).append("timeZone", timeZone).append("bio", bio).append("additionalProperties", additionalProperties).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(sisImportId).append(loginId).append(locale).append(timeZone).append(id).append(enrollments).append(lastLogin).append(bio).append(sortableName).append(email).append(additionalProperties).append(avatarUrl).append(name).append(integrationId).append(sisUserId).append(shortName).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof CanvasUser) == false) {
			return false;
		}
		CanvasUser rhs = ((CanvasUser) other);
		return new EqualsBuilder().append(sisImportId, rhs.sisImportId).append(loginId, rhs.loginId).append(locale, rhs.locale).append(timeZone, rhs.timeZone).append(id, rhs.id).append(enrollments, rhs.enrollments).append(lastLogin, rhs.lastLogin).append(bio, rhs.bio).append(sortableName, rhs.sortableName).append(email, rhs.email).append(additionalProperties, rhs.additionalProperties).append(avatarUrl, rhs.avatarUrl).append(name, rhs.name).append(integrationId, rhs.integrationId).append(sisUserId, rhs.sisUserId).append(shortName, rhs.shortName).isEquals();
	}

}