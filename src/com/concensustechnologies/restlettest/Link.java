package com.concensustechnologies.restlettest;

import org.restlet.engine.header.Header;

public class Link {
	
	private String URI = "";
	private String REL = "";

	
	
	public Link(String uRI, String rEL) {
		super();
		URI = uRI;
		REL = rEL;
	}
	
	public Link(){
		super();
	}
	
	public Link(Header hdr) {
		super();
	}

	/**
	 * @return the uRI
	 */
	public String getURI() {
		return URI;
	}

	/**
	 * @param uRI the uRI to set
	 */
	public void setURI(String uRI) {
		URI = uRI;
	}

	/**
	 * @return the rEL
	 */
	public String getREL() {
		return REL;
	}

	/**
	 * @param rEL the rEL to set
	 */
	public void setREL(String rEL) {
		REL = rEL;
	}

	
}
